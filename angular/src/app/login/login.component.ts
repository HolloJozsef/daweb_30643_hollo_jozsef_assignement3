import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {JarwisService} from "../Services/jarwis.service";
import {TokenService} from "../Services/token.service";
import {Router} from "@angular/router";
import {AuthService} from "../Services/auth.service";
import {User} from "../model/user";
import {TechService} from "../Services/tech.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

public form={
  email:null,
  password:null
};
public error = null;
  constructor(
    private Jarwis:JarwisService,
    private Token: TokenService,
    private router:Router,
    private Auth:AuthService,
  ) { }
  onSubmit(){

    this.Jarwis.login(this.form).subscribe(
      data=>this.handleResponse(data),
      error =>this.handleError(error)

    );
  }
  handleResponse(data){
     this.Token.handle(data.access_token);
     this.Auth.changeAuthStatus(true);
     this.router.navigateByUrl('/home');
    this.Token.me().subscribe(
      me => sessionStorage.setItem('user', JSON.stringify(me))
    );

  }

  handleError(error){
    this.error=error.error.error;
  }
  ngOnInit(): void {

  }

}
