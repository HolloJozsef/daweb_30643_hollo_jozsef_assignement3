import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  constructor(private router:Router,private translate: TranslateService){
    translate.setDefaultLang('en');
  }
  goContact(){
    this.router.navigate(['/contact']);
  }
  ngOnInit() {
  }

}
