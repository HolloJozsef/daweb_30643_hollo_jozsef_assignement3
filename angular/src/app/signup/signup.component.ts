import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {JarwisService} from "../Services/jarwis.service";
import {TokenService} from "../Services/token.service";
import {Router} from "@angular/router";
import {AuthService} from "../Services/auth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public form={
    email:null,
    name:null,
    password:null,
    password_confirmation:null
  };
  public error:null;
  constructor(
    private Auth:AuthService,
    private Jarwis:JarwisService,
    private Token:TokenService,
    private router:Router) { }

  onSubmit(){
   this.Jarwis.signup(this.form).subscribe(
      data=>this.handleResponse(data),
      error =>this.handleError(error)
    );
  }
  handleError(error){
    this.error=error.error.error;
  }
  handleResponse(data){

    this.Auth.changeAuthStatus(true);
    this.Token.handle(data.access_token);
    this.router.navigateByUrl('/home');
  }
  ngOnInit(): void {
  }

}
