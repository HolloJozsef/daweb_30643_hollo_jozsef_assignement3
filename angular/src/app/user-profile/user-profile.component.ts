import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material/dialog';
import { AvatarComponent } from '../avatar/avatar.component';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user = new User();


  constructor( private dialog: MatDialog,
              public dialogRef: MatDialogRef<UserProfileComponent>,
             ) { }

  ngOnInit(): void {
    this.user =  JSON.parse(sessionStorage.getItem('user'));

    console.log(this.user);

  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "100%";
    this.dialog.open(AvatarComponent, {
      data: this.user
    });
  }

}
